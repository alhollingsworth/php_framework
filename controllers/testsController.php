<?php
class TestsController extends ApplicationController{
    
    protected $test_model;
    protected $database = 'testdb';
    protected $class = 'testDb';

    public function __construct(){
        
       parent::__construct();
       
       $this->test_model = new TestModel( $this->database, $this->class );
        
        if( Config::get('debug') ){
            echo '<br />===================';
            echo '<br /><strong>testsController</strong>: construct - $this->params '; print_r($this->getParams());
            echo '<br />===================<br />';
        } 
    }
    
    public function index(){
    }
    
    public function show(){
        
        $message_id = 1;
        if ( isset($params[0]) ) {
            $message_id = $params[0];
        }
        
        $this->view_data['message_text'] = $this->test_model->getMessage($message_id);
    }
}