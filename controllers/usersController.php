<?php
class UsersController extends ApplicationController{
    
    protected $user_model;
    protected $database = 'heroku_01749ee3457e281';	
    //protected $database = 'testdb';	
    protected $class = 'UserDb';
    protected $user_id;

    public function __construct(){
        
        parent::__construct();

        try {
            $this->user_model = new UserModel( $this->database, $this->class );
        } catch(Exception $exception){
            throw $exception;
        }

    }
    
    public function setUriParameterId( $id ){
        $this->user_id = $id;
    }
    
    public function getUserId(){
        return $this->user_id;
    }
    
    protected function getUser(){
        
        // TODO: refactor to define/check route
        
        if (isset($this->user_id)) {
            $user = $this->user_model->getUser($this->user_id);
            if ( is_object($user)){
                $this->view_data['flash'] = "User {$this->user_id} was found.";
                $this->view_data['user_id'] = $this->user_id;
                $this->view_data['user_name'] = $user->getUserName();
                $this->view_data['user_email'] = $user->getUserEmail();
            } else {
                $this->view_data['flash'] = "User {$this->user_id} was not found.";
                $this->view_data['user_id'] = '';
                $this->view_data['user_name'] = '';
                $this->view_data['user_email'] = '';
                return null;
            }
        } else {
            $this->view_data['flash'] = "User {$this->user_id} was not found!";
            $this->view_data['user_id'] = '';
            $this->view_data['user_name'] = '';
            $this->view_data['user_email'] = '';
            return null;
        }        
    }
    
    // /users/index
    public function index(){
        $this->view_data['users'] = $this->user_model->getUsers();
    }
    
    // /users/show/id/1
    public function show(){
        return $this->getUser();
    }
    
    // /users/delete/id/1
    public function delete(){
        
        $user_id = $this->getUserId();
        $result = $this->user_model->deleteUser($user_id);

        Config::set('debug', false);
        if( Config::get('debug') ){
            echo '<br />===================';
            echo '<br /><strong>view: render</strong> - $this->getUriParamsId() = '; print_r($this->getUriParamsId());        
            echo '<br /><strong>view: render</strong> - $result = '; print_r($result);  
            echo '<br />===================<br />';
            Config::set('debug', false);
        }

        if ($result > 0){
            $this->view_data['flash'] = 'User deleted';
            $this->view_data['users'] = $this->user_model->getUsers();
        } else {
            $this->view_data['flash'] = 'Something went wrong with delete!';
        }
    }
    
    // /users/new
    public function create(){
        $this->view_data['user_name'] = '';
        $this->view_data['user_email'] = '';
        $this->view_data['form_button_text'] = 'Create';
        $this->view_data['form_action_uri'] = '/users/save/';
    }
    
    // /users/save/
    public function save(){
        $user = new User;
        $user->setUserName( $_REQUEST['user_name'] );
        $user->setUserEmail( $_REQUEST['user_email'] );
        $result = $this->user_model->saveUser($user);
        
        $this->view_data['users'] = $this->user_model->getUsers();
        $result > 0 ? $this->view_data['flash'] = 'User saved' : $this->view_data['flash'] = 'Something went wrong with save!';
    }
    
    // /users/edit/id/1
    public function edit(){
        $user = $this->user_model->getUser($this->user_id);
        $this->view_data['user_name'] = $user->getUserName();
        $this->view_data['user_email'] = $user->getUserEmail();
        $this->view_data['form_action_uri'] = '/users/update/'.$this->user_id;
        $this->view_data['form_button_text'] = 'Update';
    }
    
    // /users/update/id/1
    public function update(){
        $requestUser = array('user_id' => $this->user_id,
            'user_name' => $_REQUEST['user_name'],
            'user_email' => $_REQUEST['user_email']);
        $result = $this->user_model->updateUser($requestUser);

        $this->view_data['users'] = $this->user_model->getUsers();

        $result > 0 ? $this->view_data['flash'] = 'User updated' : 
            $this->view_data['flash'] = 'Something went wrong with update!';
            
    }
    
     public function verify(){
        $this->view_data['form_action_uri'] = '/users/verify/';
        $this->view_data['form_button_text'] = 'Verify';
        
        $user = new User;
       // $user  = $this->user_model->verifyUser($_REQUEST['user_email']);
        
        $requestUser = array('user_email' => $_REQUEST['user_email'],
            'verified' => 'Y');
        $user = $this->user_model->verifyUser($requestUser);

        $this->view_data['users'] = $this->user_model->getUsers();

    }
    
    public function verify2(){
         
        $this->view_data['form_action_uri'] = '/users/verify/';
        $this->view_data['form_button_text'] = 'Verify';
        
         $user = new User;
   //    $verify = $this->user_model->verifyUser($_REQUEST['user_email']);
         $user  = $this->user_model->verifyUser($_REQUEST['user_email']);
        
          Config::set('debug', true);
        if( Config::get('debug') ){
            echo '<br />===================';
            echo '<br /><strong>userscontroller: render</strong> - $user = '; print_r($user);  
            echo '<br />===================<br />';
            Config::set('debug', false);
        }
        
//      $result = (array) $user;
      
          Config::set('debug', true);
        if( Config::get('debug') ){
            echo '<br />===================';
            echo '<br /><strong>userscontroller: render</strong> - $result = '; print_r($result);  
            echo '<br />===================<br />';
            Config::set('debug', false);
        }
       
        foreach ($array as $key => $val) {
           echo $val;
        }
        
    }
    
}