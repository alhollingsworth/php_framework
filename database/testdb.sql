CREATE DATABASE IF NOT EXISTS testdb;
USE testdb;

DROP TABLE message;
CREATE TABLE IF NOT EXISTS message(
   message_id INT NOT NULL AUTO_INCREMENT,
   message_text VARCHAR(100) NOT NULL,
   user_id INT,
   PRIMARY KEY ( message_id ),
   FOREIGN KEY ( user_id ) REFERENCES user( user_id )
);

INSERT INTO message (message_text, user_id) VALUES ('Test', 1);

DROP TABLE user;
CREATE TABLE IF NOT EXISTS user(
   user_id INT NOT NULL AUTO_INCREMENT,
   user_name VARCHAR(100) NOT NULL,
   user_email VARCHAR(100) NOT NULL,
   PRIMARY KEY ( user_id )
);

INSERT INTO user (user_name, user_email) VALUES ('Alex', 'alex@example.com');