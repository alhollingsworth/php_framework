<!doctype html>
<html class="no-js" lang="en">
<head>
   <meta charset="utf-8" />
   <meta http-equiv="x-ua-compatible" content="ie=edge">
   <meta name="viewport" content="width=device-width, initial-scale=1.0" />
   <title><?= Config::get('site_name') ?></title>
   <link rel="stylesheet" href="/assets/vendore/foundation/css/foundation.css" />
   <link rel="stylesheet" href="/assets/vendore/foundation/foundation-icons/foundation-icons.css">
   <!-- app.css is for additional CSS declaration and would override foundation.css  -->
   <link rel="stylesheet" href="/assets/css/app.css" />
</head>      
<body>
   <header>
      <h1>PHP Framework</h1>
      <nav>
         <ul class="vertical medium-horizontal menu">
            <li><a href="/">Home</a></li>
            <li><a href="/messages">Messages</a></li>
            <li><a href="/users">Users</a></li>
             <li><a href="/users/verify">Verify</a></li>
         </ul>
      </nav>
   </header>
   
   <section id="main">
      <?= $this->view_data['partial_view'] ?>
   </section>
   
   <script src="/assets/vendore/foundation/js/vendor/jquery.min.js"></script>
   <script src="/assets/vendore/foundation/js/vendor/what-input.min.js"></script>
   <script src="/assets/vendore/foundation/js/foundation.min.js"></script>
   <script>
      $(document).foundation();
   </script>
   
</body>
</html>