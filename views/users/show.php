<?php
$sql= "SELECT user_name, message_text FROM user, message WHERE user.user_id = message.user_id"; 
$stmt = $pdo->query($sql); 
$row = $stmt->fetch(PDO::FETCH_ASSOC);
?>
<h1>User</h1>
<p class="callout secondary"><?= $this->view_data['flash'] ?></p>
<table>
  <tr>
    <th>No</th>
    <th>Message</th>
  </tr>
  <tr>
      <td><?= $this->view_data['user_id'] ?></td>
      <td><?= $this->view_data['user_name'] ?></td>
      <td><?= $this->view_data['user_email'] ?></td>
      <td><?= $this->view_data['message_text'] ?></td>
  </tr>
</table>
