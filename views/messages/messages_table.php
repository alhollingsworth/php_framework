<h2>Messages</h2>
<p><a href="/messages/create">Create New Message</a></p>
<p><?= $this->view_data['flash'] ?></p>
<table class="messages">
  <tr>
    <th colspan=3>Action</th>
    <th>No</th>
    <th>Message</th>
    <th>User</th>
  </tr>
  <tr>
    <?php foreach ($this->view_data['messages'] as $message): ?>
      <tr>
        <td><div class="button"><a href="<?= "/messages/show/{$message->getMessageId()}" ?>">Show</a></div></td>
        <td><div class="button"><a href="<?= "/messages/edit/{$message->getMessageId()}" ?>">Edit</a></div></td>
        <td><div class="button"><a href="<?= "/messages/delete/{$message->getMessageId()}" ?>">Delete</a></div></td>
        <td><?= $message->getMessageId() ?></td>
        <td><?= $message->getMessageText() ?></td>
        <td><?= $message->getUserId() ?></td>
      </tr>
    <?php endforeach ?>
  </tr>
</table>
