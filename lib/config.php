<?php
class Config {
    
    protected static $params = array();
    
    public static function get($key){
        return isset(self::$params[$key]) ? self::$params[$key] : null;
    }
    
    public static function set($key, $value){
        self::$params[$key] = $value;
    }
    
}