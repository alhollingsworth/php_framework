<?php

class ArrayRegistry extends AbstractRegistry

{

private $_data = array();

// save new data to the array registry

public function set($key, $value)

{

$this->_data[$key] = $value;

}

// get data from the array registry

public function get($key)

{

return isset($this->_data[$key]) ? $this->_data[$key] : NULL;

}

// clear the state of the array registry

public function clear()

{

$this->_data = array();

}

}