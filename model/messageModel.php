<?php
class MessageModel {
    
    protected $messageDb;

    public function __construct($database, $class){
        
        try {
            $this->messageDb = Db::connect( $database, $class, Config::get('servername'), 
                Config::get('username'), Config::get('password'), Config::get('db'));
        } catch(Exception $exception){
            throw $exception;
//			throw new Exception( 'MessageModel Class constructor caught an exception when trying to connect to the database. Here is the exception message:  '.$exception );
        }
        
    }
    
    public function getMessages(){
        return $this->messageDb->fetchMessages();
    }
    
    public function getMessage($message_id){
        $message = $this->messageDb->fetchMessage($message_id);
        if (is_object($message)){
            $this->view_data['flash'] = "Hurray! Message {$message_id} found!";
            $this->view_data['message_no'] = $message->getMessageId();
            $this->view_data['message_text'] = $message->getMessageText();
            $this->view_data['user_id'] = $message->getUserId();
            return $this->messageDb->fetchMessage($message_id);
        } else {
            $this->view_data['flash'] = "Message {$message_id} was not found!";
            $this->view_data['message_no'] = 0;
            $this->view_data['message_text'] = '';
            $this->view_data['user_id'] = '';
            return null;
        }        
    }
    
    public function deleteMessage($message_id){
        return $this->messageDb->deleteMessage($message_id);
    }
    
    public function updateMessage($message){
        return $this->messageDb->updateMessage($message);
    }
    
    public function saveMessage($message){
        return $this->messageDb->saveMessage($message);
    }
    
    public function getNextMessageId(){
        return $this->messageDb->getMaxMessageId() + 1;
    }
    
}