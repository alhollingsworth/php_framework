<?php
class Message{

    protected $message_id;
    protected $message_text;

    public function getMessageId(){
        return $this->message_id;
    }

    public function setMessageId($message_id){
        $this->message_id = $message_id;
    }

    public function getMessageText(){
        return $this->message_text;
    }

    public function setMessageText($message_text){
        $this->message_text = $message_text;
    }
    
    public function getUserId(){
        return $this->user_id;
    }

    public function setUserId($user_id){
        $this->user_id = $user_id;
    }

}