<?php
class UserModel {
    
    protected $userDb;

    public function __construct($database, $class){ 
        
        try {
            $this->userDb = Db::connect( $database, $class, Config::get('servername'), 
                Config::get('username'), Config::get('password'), Config::get('db'));
                //$this->userDb = Db::connect( $database, $class, $server, $username, $password, $datab, $url);
        } catch(Exception $exception){
            throw $exception;
//			throw new Exception( 'UserModel Class constructor caught an exception when trying to connect to the database. Here is the exception user:  '.$exception );
        }
        
    }
    
  public function getUsers(){
        return $this->userDb->fetchUsers();
    }
    
    public function getUser($user_id){
        $user = $this->userDb->fetchUser($user_id);
        if (is_object($user)){
            $this->view_data['flash'] = "Hurray! User {$user_id} found!";
            $this->view_data['user_id'] = $user->getUserId();
            $this->view_data['user_name'] = $user->getUserName();
            $this->view_data['user_email'] = $user->getUserEmail();
            $this->view_data['verified'] = $user->getUserVerify();
            return $this->userDb->fetchUser($user_id);
        } else {
            $this->view_data['flash'] = "User {$user_id} was not found!";
            $this->view_data['user_id'] = 0;
            $this->view_data['user_name'] = '';
            $this->view_data['user_email'] = '';
            $this->view_data['verified'] = '';
            return null;
        }        
    }
    
    public function deleteUser($user_id){
        return $this->userDb->deleteUser($user_id);
    }
    
    public function updateUser($user){
        return $this->userDb->updateUser($user);
    }
    
    public function authUser($user_email){
        return $this->userDb->authUser($user_email);
    }
    
    public function saveUser($user){
        return $this->userDb->saveUser($user);
    }
    
    public function getNextUserId(){
        return $this->userDb->getMaxUserId() + 1;
    }
    
    public function verifyUser($user){
        return $this->userDb->authUser($user);
    }
    
}