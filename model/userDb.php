<?php
class UserDb {
    
    protected $pdo = null;

    public function __construct(pdo $pdo){
        $this->pdo = $pdo;
    }

    public function getMaxUserId(){
        $stmt = $this->pdo->prepare( "SELECT MAX(user_id) FROM user" );
        $stmt->execute();
        return $stmt->fetch();
    }
    
    public function fetchUsers(){
        $stmt = $this->pdo->prepare( "SELECT * FROM user" );
        $stmt->execute();
        $stmt->setFetchMode(PDO::FETCH_CLASS, 'User');
        return $stmt->fetchAll();        
    }
    
    public function verifyUser($user_email){
        $stmt = $this->pdo->prepare( "SELECT user_email FROM user WHERE user_email = :user_email LIMIT 1" );
        $stmt->execute(array(':user_email' => $user_email));
        $stmt->setFetchMode(PDO::FETCH_CLASS, 'User');
        return $stmt->fetch();
    }

    
    public function fetchUser($user_id){
        $stmt = $this->pdo->prepare( "SELECT * FROM user WHERE user_id = :user_id LIMIT 1" );
        $stmt->execute(array(':user_id' => $user_id));
        $stmt->setFetchMode(PDO::FETCH_CLASS, 'User');
        return $stmt->fetch();
    }
    
    public function updateUser($user){
        $stmt = $this->pdo->prepare( "UPDATE user SET user_name = :user_name, user_email = :user_email WHERE user_id = :user_id" );
        $result = $stmt->execute( array(':user_id' => $user['user_id'], ':user_name' => $user['user_name'], ':user_email' => $user['user_email']) );
        return $result;        
    }
    
    public function authUser($user){
        $stmt = $this->pdo->prepare( "UPDATE user SET verified = :verified WHERE user_email = :user_email" );
        $result = $stmt->execute( array(':user_email' => $user['user_email'],':verified' => $user['verified']) );
        return $result;        
    }
    
    public function saveUser($user){
        $stmt = $this->pdo->prepare( "INSERT INTO user (user_name, user_email ) VALUES (:user_name, :user_email);" );
        $result = $stmt->execute( array(':user_name' => $user->getUserName(), ':user_email' => $user->getUserEmail()) );
        if ($result > 0) {
            $result = $this->pdo->lastInsertId();
        }
        return $result;
    }
    
    public function deleteUser($user_id){
        $stmt = $this->pdo->prepare( "DELETE FROM user WHERE user_id = :user_id LIMIT 1" );
        $result = $stmt->execute(array(':user_id' => $user_id));
        return $result;
    }
    

}

