<?php
class MessageDb {
    
    protected $pdo = null;

    public function __construct(pdo $pdo){
        $this->pdo = $pdo;
    }

    public function getMaxMessageId(){
        $stmt = $this->pdo->prepare( "SELECT MAX(message_id) FROM message" );
        $stmt->execute();
        return $stmt->fetch();
    }
    
    public function fetchMessages(){
        $stmt = $this->pdo->prepare( "SELECT * FROM message" );
        $stmt->execute();
        $stmt->setFetchMode(PDO::FETCH_CLASS, 'Message');
        return $stmt->fetchAll();        
    }
    
    public function fetchMessage($message_id){
        $stmt = $this->pdo->prepare( "SELECT message_text, user_id FROM message WHERE message_id = :message_id LIMIT 1" );
        $stmt->execute(array(':message_id' => $message_id));
        $stmt->setFetchMode(PDO::FETCH_CLASS, 'Message');
        return $stmt->fetch();
    }
    
    public function updateMessage($message){
        $stmt = $this->pdo->prepare( "UPDATE message SET message_text = :message_text, user_id = :user_id WHERE message_id = :message_id" );
        $result = $stmt->execute( array(':message_id' => $message['message_id'], ':message_text' => $message['message_text'], ':user_id' => $message['user_id']) );
        return $result;        
    }
    
    public function saveMessage($message){
        $stmt = $this->pdo->prepare( "INSERT INTO message (message_text, user_id ) VALUES (:message_text, :user_id);" );
        $result = $stmt->execute( array(':message_text' => $message->getMessageText(), ':user_id' => $message->getUserId()) );
        if ($result > 0) {
            $result = $this->pdo->lastInsertId();
        }
        return $result;
    }
    
    public function deleteMessage($message_id){
        $stmt = $this->pdo->prepare( "DELETE FROM message WHERE message_id = :message_id LIMIT 1" );
        $result = $stmt->execute(array(':message_id' => $message_id));
        return $result;
    }

}

