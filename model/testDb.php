<?php
class TestDb {
    
    protected $pdo = null;

    public function __construct(pdo $pdo)
    {
        $this->pdo = $pdo;
    }
    
    public function fetchMessage($message_id){
        
        $stmt = $this->pdo->prepare( "SELECT message_text FROM message WHERE message_id = :message_id LIMIT 1" );
        $stmt->execute(array(':message_id' => $message_id));
        $obj = $stmt->fetch(PDO::FETCH_OBJ);        
        return $obj->message_text;    
        
    }

}

